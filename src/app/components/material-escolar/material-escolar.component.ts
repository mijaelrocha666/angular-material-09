import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-material-escolar',
  templateUrl: './material-escolar.component.html',
  styleUrls: ['./material-escolar.component.css']
})
export class MaterialEscolarComponent implements OnInit {

  form!: FormGroup;
  checked: boolean = false;
  form1!: FormGroup;
  lista!: string;
  entra: string[] = [];
  lista2!: string;
  borradoAuxiliar:string = '';

  constructor(
    private fb: FormBuilder

  ) {
    this.crearFormulario();
  }

  ngOnInit(): void {
  }


  crearFormulario(): void {

    this.form = this.fb.group({
        id: [''],
        utiles: this.fb.array([[this.form1 = this.fb.group({
          aceptar: [null],
          // nombre: this.fb.array([])

          nombre: ['', [Validators.pattern(/^[0-9a-zA-zñÑ\s]+$/)]],
        })
       ]])
      }
    )
  }

  get nombreNoValido() {
    return (
      this.form1.get('nombre')?.invalid && this.form1.get('nombre')?.touched
    );
  }

  get utilesEscolares() {
    return this.form.get('utiles') as FormArray;
  }

  agregarmaterial(): void {
    const nuevo = this.fb.group({
      aceptar: [null],
      nombre: [null, Validators.pattern(/^[0-9a-zA-zñÑ\s]+$/)]
    }) 
    // this.utilesEscolares.insert(0, nuevo);
    this.utilesEscolares.push(this.fb.control(''))
    console.log(this.utilesEscolares);
    
  }

  eliminarUtil(id: number): void {
    this.utilesEscolares.removeAt(id)

  }

  eliminarUtiles(): void {
    // this.utilesEscolares.clear();
    this.form = this.fb.group(
      {
        utiles: this.fb.array([])
      }
    )
      this.form1.reset({
        nombre: ""
      })
      this.entra = [];
      this.lista2 = this.entra.toString();
  }

  limpiarCajas(): void {
   this.form1.reset({
     nombre: ""
   })
   this.entra = [];
   this.lista2 = this.entra.toString();
  }


 guardarCajas(): void {
  // console.log(this.form1.value.utiles);
  console.log(this.form1);
  console.log(this.utilesEscolares);
  

    if (this.form1.value.aceptar === true) {
      this.lista = this.form1.value.nombre;
      this.entra.push(this.lista)
      this.lista2 = this.entra.join('\n')
      this.lista2 = this.lista2.split("\n").join("<br>")
      this.form1.patchValue({
        aceptar: ""
      })
    }
  }
};
function nuevo(arg0: number, nuevo: any) {
  throw new Error('Function not implemented.');
}

